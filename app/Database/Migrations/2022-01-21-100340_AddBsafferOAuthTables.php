<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddBsafferOAuthTables extends Migration
{
    public function up()
    {
        // Oauth Clients
        $this->forge->dropTable('oauth_clients', true);
        $this->forge
            ->addField([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'null' => false,
                    'auto_increment' => true,
                ],
                'client_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'client_secret' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'redirect_uri' => [
                    'type' => 'VARCHAR',
                    'constraint' => 2000,
                    'null' => true,
                ],
                'grant_types' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'scope' => [
                    'type' => 'VARCHAR',
                    'constraint' => 4000,
                    'null' => true,
                ],
                'user_id' => [
                    'type' => 'MEDIUMINT',
                    'constraint' => 8,
                    'unsigned' => true,
                    'null' => true,
                ],
            ])
            ->addPrimaryKey('id')
            ->addForeignKey('user_id', 'users', 'id', 'RESTRICT', 'RESTRICT')
            ->createTable('oauth_clients');

        // Oauth Access Tokens
        $this->forge->dropTable('oauth_access_tokens', true);
        $this->forge
            ->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'null' => false,
                    'auto_increment' => true,
                ],
                'access_token' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                    'null' => false,
                ],
                'client_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'user_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'expires' => [
                    'type' => 'TIMESTAMP',
                    'null' => false,
                ],
                'scope' => [
                    'type' => 'VARCHAR',
                    'constraint' => 4000,
                    'null' => true,
                ],
            ])
            ->addPrimaryKey('id')
            // ->addForeignKey('user_id','users','id','RESTRICT','RESTRICT')
            // ->addForeignKey('client_id','oauth_clients','id','RESTRICT','RESTRICT')
            ->createTable('oauth_access_tokens');

        // Oauth Authorization Codes
        $this->forge->dropTable('oauth_authorization_codes', true);
        $this->forge
            ->addfield([
                'authorization_code' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                    'null' => false,
                ],
                'client_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'user_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'redirect_uri' => [
                    'type' => 'VARCHAR',
                    'constraint' => 2000,
                    'null' => true,
                ],
                'expires' => [
                    'type' => 'TIMESTAMP',
                    'null' => false,
                ],
                'scope' => [
                    'type' => 'VARCHAR',
                    'constraint' => 4000,
                    'null' => true,
                ],
                'id_token' => [
                    'type' => 'VARCHAR',
                    'constraint' => 1000,
                    'null' => true,
                ],
            ])
            ->addPrimaryKey('authorization_code')
            ->createTable('oauth_authorization_codes');

        // Oauth Refresh Tokens
        $this->forge->dropTable('oauth_refresh_tokens', true);
        $this->forge
            ->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'null' => false,
                    'auto_increment' => true,
                ],
                'refresh_token' => [
                    'type' => 'VARCHAR',
                    'constraint' => 40,
                    'null' => false,
                ],
                'client_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'user_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'expires' => [
                    'type' => 'TIMESTAMP',
                    'null' => false,
                ],
                'scope' => [
                    'type' => 'VARCHAR',
                    'constraint' => 4000,
                    'null' => true,
                ],
            ])
            ->addPrimaryKey('id')
            ->createTable('oauth_refresh_tokens');

        // Oauth Users
        $this->forge->dropTable('oauth_users', true);
        $this->forge
            ->addfield([
                'username' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'password' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'first_name' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'last_name' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'email' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'email_verified' => [
                    'type' => 'BOOLEAN',
                    'null' => true,
                ],
                'scope' => [
                    'type' => 'VARCHAR',
                    'constraint' => 4000,
                    'null' => true,
                ],
            ])
            ->addPrimaryKey('username')
            ->createTable('oauth_users');

        // Oauth Scopes
        $this->forge->dropTable('oauth_scopes', true);
        $this->forge
            ->addfield([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'null' => false,
                    'auto_increment' => true,
                ],
                'scope' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'is_default' => [
                    'type' => 'BOOLEAN',
                    'null' => true,
                ],
            ])
            ->addPrimaryKey('id')
            ->createTable('oauth_scopes');

        // Oauth Jwt
        $this->forge->dropTable('oauth_jwt', true);
        $this->forge
            ->addfield([
                'client_id' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => false,
                ],
                'subject' => [
                    'type' => 'VARCHAR',
                    'constraint' => 80,
                    'null' => true,
                ],
                'public_key' => [
                    'type' => 'VARCHAR',
                    'constraint' => 2000,
                    'null' => false,
                ],
            ])
            ->createTable('oauth_jwt');
    }

    public function down()
    {
        $drop_order = [
            'oauth_jwt',
            'oauth_scopes',
            'oauth_users',
            'oauth_refresh_tokens',
            'oauth_authorization_codes',
            'oauth_access_tokens',
            'oauth_clients',
        ];

        foreach ($drop_order as $table) {
            $this->forge->dropTable($table);
        }
    }
}
