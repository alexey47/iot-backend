<?php

namespace App\Models;

use CodeIgniter\Model;

class ThingsModel extends Model
{
    protected $table = 'things';
    public function getThings($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
