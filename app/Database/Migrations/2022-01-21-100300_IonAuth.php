<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class IonAuth extends Migration
{
    public function up()
    {
        // Groups
        $this->forge->dropTable('groups', true);
        $this->forge
            ->addField([
                'id' => [
                    'type'           => 'MEDIUMINT',
                    'constraint'     => 8,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],
                'name' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 20,
                ],
                'description' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 100,
                ],
            ])
            ->addPrimaryKey('id')
            ->createTable('groups');

        // Users
        $this->forge->dropTable('users', true);
        $this->forge
            ->addField([
                'id' => [
                    'type'           => 'MEDIUMINT',
                    'constraint'     => 8,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],
                'ip_address' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 45,
                ],
                'username' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 100,
                ],
                'password' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ],
                'email' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 254,
                    'unique'     => true,
                ],
                'activation_selector' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'null'       => true,
                    'unique'     => true,
                ],
                'activation_code' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'null'       => true,
                ],
                'forgotten_password_selector' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'null'       => true,
                    'unique'     => true,
                ],
                'forgotten_password_code' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'null'       => true,
                ],
                'forgotten_password_time' => [
                    'type'       => 'INT',
                    'constraint' => 11,
                    'unsigned'   => true,
                    'null'       => true,
                ],
                'remember_selector' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'null'       => true,
                    'unique'     => true,
                ],
                'remember_code' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'null'       => true,
                ],
                'created_on' => [
                    'type'       => 'INT',
                    'constraint' => 11,
                    'unsigned'   => true,
                ],
                'last_login' => [
                    'type'       => 'INT',
                    'constraint' => 11,
                    'unsigned'   => true,
                    'null'       => true,
                ],
                'active' => [
                    'type'       => 'TINYINT',
                    'constraint' => 1,
                    'unsigned'   => true,
                    'null'       => true,
                ],
                'first_name' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 50,
                    'null'       => true,
                ],
                'last_name' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 50,
                    'null'       => true,
                ],
                'company' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 100,
                    'null'       => true,
                ],
                'phone' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 20,
                    'null'       => true,
                ],
            ])
            ->addPrimaryKey('id')
            ->createTable('users');

        // Users groups
        $this->forge->dropTable('users_groups', true);
        $this->forge
            ->addField([
                'id' => [
                    'type'           => 'MEDIUMINT',
                    'constraint'     => 8,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],
                'user_id' => [
                    'type'       => 'MEDIUMINT',
                    'constraint' => 8,
                    'unsigned'   => true,
                ],
                'group_id' => [
                    'type'       => 'MEDIUMINT',
                    'constraint' => 8,
                    'unsigned'   => true,
                ],
            ])
            ->addPrimaryKey('id')
            ->addForeignKey('user_id', 'users', 'id', 'NO ACTION', 'CASCADE')
            ->addForeignKey('group_id', 'groups', 'id', 'NO ACTION', 'CASCADE')
            ->createTable('users_groups');

        // Login attempts
        $this->forge->dropTable('login_attempts', true);
        $this->forge
            ->addField([
                'id' => [
                    'type'           => 'MEDIUMINT',
                    'constraint'     => 8,
                    'unsigned'       => true,
                    'auto_increment' => true,
                ],
                'ip_address' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 45,
                ],
                'login' => [
                    'type'       => 'VARCHAR',
                    'constraint' => 100,
                    'null'       => true,
                ],
                'time' => [
                    'type'       => 'INT',
                    'constraint' => 11,
                    'unsigned'   => true,
                    'null'       => true,
                ]
            ])
            ->addPrimaryKey('id')
            ->createTable('login_attempts', true);
    }

    public function down()
    {
        $drop_order = [
            'login_attempts',
            'users_groups',
            'users',
            'groups',
        ];

        foreach ($drop_order as $table) {
            $this->forge->dropTable($table);
        }
    }
}
