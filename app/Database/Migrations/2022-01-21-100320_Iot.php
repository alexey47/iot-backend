<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Iot extends Migration
{
    public function up()
    {
        // Things
        $this->forge->dropTable('things', true);
        $this->forge
            ->addField([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'null' => false,
                    'auto_increment' => true,
                ],
                'name' => [
                    'type' => 'VARCHAR',
                    'constraint' => 128,
                    'null' => false,
                ],
                'description' => [
                    'type' => 'TEXT',
                    'null' => true,
                ],
                'user_id' => [
                    'type' => 'MEDIUMINT',
                    'constraint' => 8,
                    'unsigned' => true,
                    'null' => false,
                ],
            ])
            ->addPrimaryKey('id')
            ->addForeignKey('user_id', 'users', 'id', 'RESTRICT', 'RESRICT')
            ->createTable('things', true);
    }

    public function down()
    {
        $drop_order = [
            'things',
        ];

        foreach ($drop_order as $table) {
            $this->forge->dropTable($table);
        }
    }
}
