<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class BsafferOAuth extends Seeder
{
    public function run()
    {
        $client = [
            'client_id' => 'TestClient',
            'client_secret' => 'test_secret'
        ];
        $this->db->table('oauth_clients')->insert($client);
    }
}
