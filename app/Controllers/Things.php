<?php

namespace App\Controllers;

use App\Models\ThingsModel;

class Things extends BaseController
{
    public function index($id = null)
    {
        $model = new ThingsModel();
        $data['things'] = $model->getThings($id);

        if (isset($id)) {
            // echo view('things/view', $data);
            echo ($data);
        } else {
            // echo view('things/view_all', $data);
            echo ($data);
        }
    }
    // public function view($id = null) {
    //     $model = new ThingsModel();
    //     $data['things'] = $model->getThings($id);
    //     echo view('things/view', $data);
    // }
}
