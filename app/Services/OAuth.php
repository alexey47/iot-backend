<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public $server;

    function __construct()
    {
        $storage = new MyPdo(
            [
                'dsn' => $_ENV['database.default.DSN']
            ],
            [
                'user_table' => 'users'
            ],
        );
        $this->server = new Server($storage);
        $this->server->addGrantType(new UserCredentials($storage));
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }
}
